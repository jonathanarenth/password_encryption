package fr.afpa.pompey.cda02.examples;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;

import org.apache.commons.codec.digest.DigestUtils;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import de.mkammerer.argon2.Argon2Factory.Argon2Types;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Main
{
    /**
     * Le poivre, une valeur arbitraire qu'on va mêler au mot de passe avant
     * de le traiter. Quinconque voudrait "décrypter" un mot de passe en
     * base aurait besoin de connaître cette valeur
     */
    private static String PEPPER;

    public static void main(String[] args)
    {
        Main.PEPPER = System.getenv("BLOGGY_APP_SECRET");
        if (Main.PEPPER == null) {
            throw new RuntimeException("BLOGGY_APP_SECRET is not set in the environement");
        }
        Main app = new Main();
        app.start();
    }

    public void start() {
        // Représente un mot de passe qu'on cherche à sauvegarder pour
        // un utilisateur
        String rawPassword = "123456";

        // Représente la soumission valide d'un utilisateur qui cherche
        // à s'authentifier et qu'on va comparer au premier
        String validSumittedPassword = "123456";

        // Représente la soumission _invalide_ d'un utilisateur qui cherche
        // à s'authentifier et qu'on va comparer au premier
        // String sumittedPassword = "654321";
        String invalidSumittedPassword = "654321";

        String encryptedPassword;
        boolean passwordMatch;

        log.info("Pepper: {}", PEPPER);
        log.info("Raw Password: <{}>", rawPassword);

        log.info("Handcraft password encryption, using SHA-256, salt and pepper");
        // On crypte le mot de passe, un sel est généré automatiquement
        encryptedPassword = encryptPassword(rawPassword + PEPPER);
        log.info("Encrypted password: <{}>", encryptedPassword);
        passwordMatch = verifyPassword(
            encryptedPassword,
            validSumittedPassword + PEPPER
        );
        log.info("Submitted Valid Password: <{}>", rawPassword);
        log.info("Passwords match: {}", passwordMatch);

        passwordMatch = verifyPassword(
            encryptedPassword,
            invalidSumittedPassword + PEPPER
        );
        log.info("Submitted InValid Password: <{}>", validSumittedPassword);
        log.info("Passwords match: {}", passwordMatch);


        // Argon2
        log.info("Same thing, but using argon2-java");
        Argon2 argon2 = Argon2Factory.create(Argon2Types.ARGON2id);
        encryptedPassword = argon2.hash(4, 1024 * 1024, 8, rawPassword + PEPPER);
        log.info("Encrypted password: <{}>", encryptedPassword);

        passwordMatch = argon2.verify(
            encryptedPassword,
            validSumittedPassword + PEPPER
        );
        log.info("Submitted Valid Password: <{}>", rawPassword);
        log.info("Passwords match: {}", passwordMatch);

        passwordMatch = argon2.verify(
            encryptedPassword,
            invalidSumittedPassword + PEPPER
        );
        log.info("Submitted InValid Password: <{}>", invalidSumittedPassword);
        log.info("Passwords match: {}", passwordMatch);
    }

    private static final String extractSalt(String encryptedPassword) {
        String[] parts = encryptedPassword.split("\\$");

        if (parts.length < 3) {
            log.error(
                "Hash seems malformed [hash={}] [parts={}]",
                encryptedPassword,
                Arrays.toString(parts)
            );
        }

        return parts[1];
    }

    private static final String encryptPassword(String password) {
        String salt = generateRandomString(30);
        return encryptPassword(password, salt);
    }

    private static final String encryptPassword(String password, String salt) {
        String hash = hashWithSha256(password + salt + PEPPER);

        String result = String.format(
            "sha256$%s$%s",
            salt,
            hash
        );

        return result;
    }

    private static final String hashWithSha256(String password) {
        String hash = DigestUtils.sha256Hex(password);
        log.info("Hash: <{}> to <{}>", password, hash);
        return hash;
    }

    private static final boolean verifyPassword(String encryptedPassword, String password) {
        String salt = extractSalt(encryptedPassword);

        return encryptedPassword.equals(encryptPassword(password, salt));
    }

    public static final String generateRandomString(int length) {
        Random rng = new SecureRandom();

        String charPool = "0123456789"
            + "abcdefghijklmnopqrstuvwxyz"
            + "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < length; i++) {
            int randIndex = rng.nextInt(charPool.length());
            builder.append(charPool.charAt(randIndex));
        }

        return builder.toString();
    }
}
